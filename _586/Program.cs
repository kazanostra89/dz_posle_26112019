﻿using static System.Console;

namespace _586
{
    class Program
    {
        static void Main()
        {
            int n, Kn, summa, expression;

            summa = 0;
            Kn = 1;

            Write("Ведите число n: ");
            n = System.Convert.ToInt32(ReadLine());

            while (Kn <= n)
            {
                expression = 2 * Kn - 1;
                summa = summa + expression;
                Kn = Kn + 1;
            }

            WriteLine("Возведение в квадрат по закономерности " + summa);

            ReadKey();
        }
    }
}
