﻿using static System.Console;

namespace _538
{
    class Program
    {
        static void Main()
        {
            int Fstage, Lstage;
            double road, Sroad;
            road = 1.0;
            Fstage = 1; // первый этап, он же счетчик этапов
            Sroad = 0.0;  // сумма пути
            Write("Введите число этапов: ");
            Lstage = System.Convert.ToInt32(ReadLine());    // задаваемый максимальный этап

            while (Fstage <= Lstage)
            {
                road = 1.0 / (double) Fstage;       // расстояние на этапах + явное приведение типа перем. Fstage
                Sroad = Sroad + road;               // сумма пройденого пути
                Fstage = Fstage + 1;
                
            }

            WriteLine("\nОбщий путь = " + Sroad + "\n\nРасстояние на этапе 100 = " + road);
            ReadKey();
        }
    }
}
