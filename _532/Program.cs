﻿using static System.Console;

namespace _532
{
    class Program
    {
        static void Main()
        {
            double aN, bN, delen, summa;
            aN = 1;
            summa = 0;
            Write("Введите число N: ");
            bN = System.Convert.ToDouble(ReadLine());
            
            while (aN <= bN)
            {
                delen = 1 / aN;
                summa = summa + delen;
                aN = aN + 1;
            }

            WriteLine("Сумма дробей равна: " + summa);
            ReadKey();
        }
    }
}
