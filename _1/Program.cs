﻿using static System.Console;

namespace _1
{
    class Program
    {
        static void Main()
        {
            int a, b, chislo, x5, y7;
            a = 1;
            b = 10;
            x5 = y7 = 0;
            
            while (a <= b)
            {
                Write(a + " Введите число: ");
                chislo = System.Convert.ToInt32(ReadLine());

                if (chislo == 5)
                {
                    x5 = x5 + 1;
                }
                else if (chislo == 7)
                {
                    y7 = y7 + 1;
                }

                a = a + 1;
                
            }

            if (x5 > y7)
            {
                WriteLine("\nЧисло 5 (" + x5 + ") встречается чаще числа 7 (" + y7 + ")");
            }
            else if (x5 < y7)
            {
                WriteLine("\nЧисло 7 (" + y7 + ") встречается чаще числа 5 (" + x5 + ")");
            }
             else
            {
                WriteLine("\nЧисло 5 (" + x5 + ") встречается столько же, как и число 7 (" + y7 + ")");
            }
                        
            ReadKey();
        }
    }
}
